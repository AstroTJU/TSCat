from django.db import models

# Create your models here.

class User(models.Model):
    username = models.CharField("Name", max_length=30, unique=True)
    password = models.CharField("Password", max_length=32)
    created_time = models.DateTimeField("Create_Time", auto_now_add=True)
    updated_time = models.DateTimeField("Upadted_Time", auto_now=True)
    is_active = models.BooleanField('Is_active', default=False)

    def __str__(self):
        return 'username %s'%(self.username)
from django.http import HttpResponse
from django.shortcuts import render
from lightcurve.models import Ast3Data, Ast3Meta
import csv
from upload_app.models import Content
import os
from django.conf import settings

def home(request):

    return render(request, 'home.html')

def search(request):
    if request.method == 'GET':
        return render(request, 'search.html')

    elif request.method == 'POST':
        RA = request.POST['RA']
        DEC = request.POST['DEC']
        radius = request.POST['search_radius']

        result = Ast3Meta.objects.filter(field_ra_field=RA)
        if result.exists():
            for res in result:
                data = Ast3Data.objects.filter(field_match_id_field=res.field_match_id_field)
                x = []
                y = []
                z = []
                for i in data:
                    x.append(i.field_date_obs_field.strftime('%Y-%m-%d %H:%I:%S'))
                    y.append(i.field_mag_field)
                    z.append(i.field_mag_err_field)
                match_id = res.field_match_id_field
            return render(request, 'search.html', locals())
        else:
            return HttpResponse("No matched data!")

def download_csv(request):
    if request.method == 'GET':
        return render(request, 'search.html')

    elif request.method == 'POST':
        RA = request.POST['RA']
        DEC = request.POST['DEC']
        radius = request.POST['search_radius']

        result = Ast3Meta.objects.filter(field_ra_field=RA)
        if result.exists():
            for res in result:
                data = Ast3Data.objects.filter(field_match_id_field=res.field_match_id_field)
                response = HttpResponse(content_type='text/csv')
                response['content-Disposition'] = 'attachment;filename="source-%s.csv"'%(res.field_match_id_field)
                writer = csv.writer(response)
                writer.writerow(['Match_ID', 'DATE_OBS', 'X', 'Y', 'Mag', 'Mag_error', 'Provenance'])
                for b in data:
                    writer.writerow([b.field_match_id_field, b.field_date_obs_field, b.field_x1_field, b.field_y1_field, b.field_mag_field, b.field_mag_err_field, b.field_provenance_field])
                return response
        else:
            return render(request, 'search.html')

def upload_csv(request):
    if request.method == 'GET':
        return render(request, 'sources.html')
    elif request.method == 'POST':
        title = request.POST['title']
        my_file = request.FILES['my_file']
        Content.objects.create(title=title, file=my_file)
        with open(os.path.join(settings.MEDIA_ROOT, my_file.name), 'wb') as f:
             data = my_file.file.read()
             f.write(data)
             print(data)
        return HttpResponse("OK!")

def sources(request):

    return render(request, 'sources.html')
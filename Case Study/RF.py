import numpy as np
import scipy
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import label_binarize

from sklearn.model_selection import train_test_split, cross_val_score, GridSearchCV
from sklearn.metrics import roc_curve, auc, roc_auc_score

import matplotlib.pyplot as plt

df = pd.read_csv("/home/allen/PycharmProjects/data/lightcurves.csv")
class_dict = {'CEP': 0, 'DSCT': 1, 'EB': 2, 'LPV': 3, 'ROT': 4, 'RR': 5}
df['label'] = df['label'].map(class_dict)
df = df[df['label'].isin([0, 1, 2, 3, 4, 5])].sample(frac=1, random_state=66).reset_index(drop=True)
y = df.label
x = df.drop('label', axis=1)
seed = 5
xtrain, xtest, ytrain, ytest = train_test_split(x, y, test_size=0.3, random_state=seed)

param_test1 = {'n_estimators': range(25, 200, 25)}
gsearch1 = GridSearchCV(estimator = RandomForestClassifier(min_samples_split=10, min_samples_leaf=10, max_depth=5, random_state=0), param_grid=param_test1, scoring='accuracy', cv=5)
gsearch1.fit(xtrain, ytrain)
print(gsearch1.best_params_, gsearch1.best_score_)

param_test2 = {'min_samples_split':range(2, 10, 1), 'min_samples_leaf': range(2, 10, 1)}
gsearch2 = GridSearchCV(estimator = RandomForestClassifier(n_estimators=75, max_depth=8, random_state=0), param_grid=param_test2, scoring='accuracy', cv=5)
gsearch2.fit(xtrain, ytrain)
print(gsearch2.best_params_, gsearch2.best_score_)

param_test3 = {'max_depth':range(2, 6, 1)}
gsearch3 = GridSearchCV(estimator = RandomForestClassifier(n_estimators=75, min_samples_split=60, min_samples_leaf=10, random_state=0), param_grid=param_test3, scoring='accuracy', cv=5)
gsearch3.fit(xtrain,ytrain)
print(gsearch3.best_params_, gsearch3.best_score_)

param_test4 = {'criterion':['gini', 'entropy'], 'class_weight':[None, 'balanced']}
gsearch4 = GridSearchCV(estimator = RandomForestClassifier(n_estimators=100, max_depth=5, min_samples_split=8, min_samples_leaf=3, random_state=0), param_grid=param_test4, scoring='accuracy', cv=5)
gsearch4.fit(xtrain,ytrain)
print(gsearch4.best_params_, gsearch4.best_score_)

rfc = RandomForestClassifier(n_estimators=50, max_depth=5, random_state=0)
rfc = rfc.fit(xtrain, ytrain)

result = rfc.score(xtest, ytest)
print(result)
scores = cross_val_score(rfc, xtrain, ytrain)
print(scores.mean())

importances = rfc.feature_importances_
std = np.std([tree.feature_importances_ for tree in rfc.estimators_], axis=0)
indices = np.argsort(importances)[::-1] # Print the feature ranking
print("Feature ranking:")
for f in range(min(20,xtrain.shape[1])):
    print("%2d) %-*s %f" % (f + 1, 30, xtrain.columns[indices[f]], importances[indices[f]])) # Plot the feature importances of the forest
plt.figure()
plt.title("Feature importances")
plt.bar(range(xtrain.shape[1]), importances[indices],  color="r", yerr=std[indices], align="center")
plt.xticks(range(xtrain.shape[1]), indices)
plt.xlim([-1, xtrain.shape[1]])
plt.show()

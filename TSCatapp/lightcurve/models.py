from django.db import models


class Ast3ColDesc(models.Model):
    name = models.CharField(primary_key=True, max_length=30)
    datatype = models.CharField(max_length=10, blank=True, null=True)
    unit = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AST3_col_desc'


class Ast3Data(models.Model):
    field_date_obs_field = models.DateTimeField(db_column='_DATE_OBS_')  # Field name made lowercase. Field renamed because it started with '_'. Field renamed because it ended with '_'.
    field_x1_field = models.FloatField(db_column='_X1_', blank=True, null=True)  # Field name made lowercase. Field renamed because it started with '_'. Field renamed because it ended with '_'.
    field_y1_field = models.FloatField(db_column='_Y1_', blank=True, null=True)  # Field name made lowercase. Field renamed because it started with '_'. Field renamed because it ended with '_'.
    field_mag_field = models.FloatField(db_column='_mag_', blank=True, null=True)  # Field renamed because it started with '_'. Field renamed because it ended with '_'.
    field_mag_err_field = models.FloatField(db_column='_mag_err_', blank=True, null=True)  # Field renamed because it started with '_'. Field renamed because it ended with '_'.
    field_provenance_field = models.CharField(db_column='_Provenance_', max_length=20, blank=True, null=True)  # Field name made lowercase. Field renamed because it started with '_'. Field renamed because it ended with '_'.
    field_match_id_field = models.CharField(db_column='_Match_ID_', primary_key=True, max_length=20)  # Field name made lowercase. Field renamed because it started with '_'. Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'AST3_data'
        unique_together = (('field_match_id_field', 'field_date_obs_field'),)

    def __str__(self):
        return "%s_%s_%s_%s"%(self.field_date_obs_field, self.field_x1_field, self.field_mag_err_field, self.field_match_id_field)


class Ast3Meta(models.Model):
    field_zone_id_field = models.CharField(db_column='_Zone_ID_', primary_key=True, max_length=14)  # Field name made lowercase. Field renamed because it started with '_'. Field renamed because it ended with '_'.
    field_match_id_field = models.CharField(db_column='_Match_ID_', max_length=16)  # Field name made lowercase. Field renamed because it started with '_'. Field renamed because it ended with '_'.
    field_ra_field = models.CharField(db_column='_RA_', max_length=20, blank=True, null=True)  # Field name made lowercase. Field renamed because it started with '_'. Field renamed because it ended with '_'.
    field_dec_field = models.CharField(db_column='_DEC_', max_length=20, blank=True, null=True)  # Field name made lowercase. Field renamed because it started with '_'. Field renamed because it ended with '_'.
    col_desc_table = models.CharField(max_length=128, blank=True, null=True)
    data_table = models.CharField(max_length=128, blank=True, null=True)
    count = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AST3_meta'
        unique_together = (('field_zone_id_field', 'field_match_id_field'),)


class Dataset(models.Model):
    dataset = models.CharField(primary_key=True, max_length=64)
    meta_table = models.CharField(max_length=128, blank=True, null=True)
    col_desc_table = models.CharField(max_length=30, blank=True, null=True)
    count = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dataset'
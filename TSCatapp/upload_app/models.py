from django.db import models

# Create your models here.

class Content(models.Model):

    title = models.CharField('filename', max_length=20)
    file = models.FileField(upload_to='file')
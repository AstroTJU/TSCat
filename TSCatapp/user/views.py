from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from .models import User
import hashlib

# Create your views here.

def reg_view(request):
    if request.method == 'GET':
        return render(request, 'register.html')
    elif request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        password_1 = request.POST['password_1']

        if password != password_1:
            return HttpResponse('Two inconsistent passwords!')

        m = hashlib.md5()
        m.update(password.encode())
        password_m = m.hexdigest()

        old_users = User.objects.filter(username=username)
        if old_users:
            return HttpResponse('Username has been registered!')
        try:
            user = User.objects.create(username=username, password=password_m)
        except Exception as e:
            print('--create user error %s'%(e))
            return HttpResponse('Username has been registered!')

        request.session['username'] = username
        request.session['uid'] = user.id

        return HttpResponseRedirect('/home')

def login_view(request):
    if request.method == 'GET':
        if request.session.get('username') and request.session.get('uid'):
            #return HttpResponse('Logged')
            return HttpResponseRedirect('/home')
        c_username = request.COOKIES.get('username')
        c_uid = request.COOKIES.get('uid')
        if c_username and c_uid:
            request.session['username'] = c_username
            request.session['uid'] = c_uid
            #return HttpResponse('Logged')
            return HttpResponseRedirect('/home')
        return render(request, 'login.html')
    elif request.method == 'POST':

        username = request.POST['username']
        password = request.POST['password']

        try:
            user = User.objects.get(username=username)
        except Exception as e:
            print('---login user error %s'%(e))
            return HttpResponse('Your username or password is incorrect!')

        m = hashlib.md5()
        m.update(password.encode())

        if m.hexdigest() != user.password:
            return HttpResponse('Your username or password is incorrect!')

        request.session['username'] = username
        request.session['uid'] = user.id

        resp = HttpResponseRedirect('/home')

        if 'remember' in request.POST:
            resp.set_cookie('username', username, 3600*24*3)
            resp.set_cookie('uid', user.id, 3600*24*3)

        return resp

def logout_view(request):

    if 'username' in request.session:
        del request.session['username']
    if 'uid' in request.session:
        del request.session['uid']
    resp = HttpResponseRedirect('/home')
    if 'username' in request.COOKIES:
        resp.delete_cookie('username')
    if 'uid' in request.COOKIES:
        resp.delete_cookie('uid')

    return resp

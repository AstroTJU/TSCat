from django.apps import AppConfig


class LightcurveConfig(AppConfig):
    name = 'lightcurve'
